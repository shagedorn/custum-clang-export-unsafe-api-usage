//===- APIExporter.mm - Replicates APIExporterCWrapper for ObjC -*- ObjC -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
///
/// \file
/// \brief Defines export functionality in Objective-C
///
/// Bridges between C API and ObjC implementation.
///
//===----------------------------------------------------------------------===//

#import "clang/basic/APIExporter.h"

#pragma mark - Private Interface

// Singleton instance
static APIExporter *_instance;

@interface APIExporter ()

+ (APIExporter*) getInstance;

@property (retain) NSMutableArray *symbols;

- (NSString*) exportOutputPath:(NSString*)objectsOutputPath;

@end

#pragma mark -

@implementation APIExporter{}

#pragma mark - C Wrapper functions

void AEWExportSymbols(const char* path) {
  NSString *pathString = nil;
  if (path != NULL) {
     pathString = [NSString stringWithUTF8String:path];
  }
  [[APIExporter getInstance] exportSymbols:pathString];
}

void AEWAddSymbol(AEWSymbolType symbolType,
                  AEWSymbolSourceLocation sourceCodeLocation,
                  AEWiOSVersion introducedIniOSVersion,
                  const char* symbolName,
                  AEWSymbolContainer symbolContainer,
                  const char* superClassName,
                  std::string returnTypeName,
                  std::vector< std::vector<std::string> > params,
                  bool declaredByUncriticalClass = false) {

  // Type conversion, if necessary
  APIExporterSymbol *newSymbol = [[APIExporterSymbol alloc] init];
  newSymbol.type = symbolType;
  newSymbol.location = sourceCodeLocation;
  newSymbol.minSDKVersion = introducedIniOSVersion;
  newSymbol.name = @(symbolName);
  newSymbol.superclassName = (symbolType == AEWSymbolTypeSubclass)? @(superClassName) : @"";
  newSymbol.container = symbolContainer;
  newSymbol.returnTypeName = @(returnTypeName.c_str());
  newSymbol.declarationIsUncritical = declaredByUncriticalClass;

  int size = params.size();
  NSMutableArray *allParams = [NSMutableArray arrayWithCapacity:size];
  for (int i = 0; i < size; i++) {
    std::vector<std::string> oneParam = params[i];
    NSArray *par = @[@(oneParam[0].c_str()), @(oneParam[1].c_str())];
    [allParams addObject:par];
  }
  newSymbol.params = [NSArray arrayWithArray:allParams];

  [[APIExporter getInstance] addSymbol:newSymbol];
}

#pragma mark - Objective-C only!

#pragma mark - Singleton & Lifecycle

+ (APIExporter *)getInstance {
  if (!_instance) {
    _instance = [[APIExporter alloc] init];
  }
  return _instance;
}

- (id)init {
  if (self = [super init]) {
    self.symbols = [NSMutableArray arrayWithCapacity:100];
  }
  return self;
}

- (void)dealloc {
  self.symbols = nil;
  [_instance release]; // will never happen...
  [super dealloc];
}

#pragma mark - Actual implementation

- (void)exportSymbols:(NSString*)path {
  if (!path) {
    return;
  }
  path = [self exportOutputPath:path];

  
  NSFileManager *fileManager = [NSFileManager defaultManager];
  if (![fileManager fileExistsAtPath:path]) {
    // Create new directory for output
    NSError *error = nil;
    [fileManager createDirectoryAtPath:path
           withIntermediateDirectories:YES
                            attributes:nil
                                 error:&error];
    if (error) {
      NSLog(@"Error creating folder: %@", error);
    }
  }

  // Only write back if content has been changed
  BOOL changesMade = NO;
  
  static NSString *exportFilename = @"symbols.plist";
  NSString *fullExportPath = [path stringByAppendingPathComponent:exportFilename];
  NSMutableDictionary *dict = [[NSDictionary dictionaryWithContentsOfFile:fullExportPath] mutableCopy];
  if (!dict) {
    // File is not present - create new file (content)
    dict = [NSMutableDictionary dictionaryWithCapacity:self.symbols.count];
    changesMade = YES;
  }

  for (APIExporterSymbol *currentSymbol in self.symbols) {

    // same symbol found before?
    NSMutableDictionary *symbolInfo = [[dict objectForKey:currentSymbol.symbolKey] mutableCopy];
    if (symbolInfo) {
      // found before... just add location information, if necessary
      BOOL hasChangedNow = [currentSymbol mergeIntoDictionary:symbolInfo];
      if (hasChangedNow) {
        changesMade = YES;
        [dict setObject:symbolInfo forKey:currentSymbol.symbolKey];
      }
    } else {
      // not found - create new symbol entry
      changesMade = YES;
      // cast to suppress warnings - do not access this dictionary anymore
      symbolInfo = (NSMutableDictionary*)[currentSymbol fullDictionary];
      [dict setObject:symbolInfo forKey:currentSymbol.symbolKey];
    }

  }

  // the dictionary is now complete - write back, if any changes have been made to it
  if (changesMade) {
    BOOL success = [dict writeToFile:fullExportPath atomically:NO];
    if (!success) {
      NSLog(@"Failed writing to %@", fullExportPath);
    } else {
      NSLog(@"Successfully exported symbols to %@", fullExportPath);
    }
  } else {
    NSLog(@"No changes in export symbols");
  }
  
}

- (void)addSymbol:(APIExporterSymbol*)symbolToAdd {
  [self.symbols addObject:symbolToAdd];
}

- (NSString*) exportOutputPath:(NSString*)objectsOutputPath {

  // Example: ~/Library/Developer/Xcode/DerivedData/BWC-ahbeuudjsmiorqgtrptnvvspjusi/
  //          Build/Intermediates/BWC.build/Debug-iphoneos/BWC.build/Objects-normal/armv7/ViewController.o
  //                                                                ^
  //                                              We want to go there

  NSArray *pathComponents = [objectsOutputPath pathComponents];
  int intermediatesDirectoryIndex = [pathComponents indexOfObject:@"Intermediates"];

  // go to a folder that is wiped when you 'Clean' the project in Xcode
  intermediatesDirectoryIndex++; // jump into 'Intermediates'
  intermediatesDirectoryIndex++; // 1up: '<app>.build'
  intermediatesDirectoryIndex++; // 1up: '<Configuration>-<Platform>'
  intermediatesDirectoryIndex++; // 1up: '<app>.build'

  NSString *rangeString = [NSString stringWithFormat:@"0-%d", intermediatesDirectoryIndex];
  pathComponents = [pathComponents subarrayWithRange:NSRangeFromString(rangeString)];

  NSString *finalPath = [pathComponents componentsJoinedByString:@"/"];
  return [finalPath stringByAppendingPathComponent:@"APISymbolsExport"];
}


@end

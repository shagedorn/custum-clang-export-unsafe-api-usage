//===--- APIExporterSymbol.m - Representation of an API symbol -*- ObjC -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
///
/// \file
/// \brief Defines a symbol that may be exported for analysis purposes.
///
//===----------------------------------------------------------------------===//

#import "clang/basic/APIExporterSymbol.h"

#pragma mark - Private Interface

@interface APIExporterSymbol ()

- (NSDictionary*) createLocationDict;

@end

#pragma mark - Implementation

@implementation APIExporterSymbol

- (void)dealloc {
  self.superclassName = nil;
  self.name = nil;
  [super dealloc];
}

#pragma mark - Debug Helper

- (NSString *)debugDescription {
  return [NSString stringWithFormat:@"\n\nExport Symbol (%@) '%@':\nReferenced at: [%s:%d]\nType: %@\nSince: %d.%d\nDeclared in: %s (%@)\n\n",
          self.returnTypeName,
          self.name,
          self.location.filename,
          self.location.lineNumberInFile,
          [APIExporterSymbol nameForSymbolType:self.type],
          self.minSDKVersion.major,
          self.minSDKVersion.minor,
          self.container.containerName,
          [APIExporterSymbol nameForContainerType:self.container.type]];
}

#pragma mark - Identification/Export

- (NSString *)symbolKey {
  NSString *version = [NSString stringWithFormat:@"%d.%d", self.minSDKVersion.major, self.minSDKVersion.minor];
  NSString *name = [self.name lastPathComponent];
  NSString *containerName = [[NSString stringWithUTF8String:self.container.containerName] lastPathComponent];
  NSMutableString *paramStr = [NSMutableString string];
  for (NSArray *oneParam in self.params) {
    [paramStr appendFormat:@",%@", oneParam[0]];
  }

  return [NSString stringWithFormat:@"%d<<##>>%@<<##>>%@<<##>>%@<<##>>%d<##>%@<##>%@<##>%@", self.type, name, version, containerName, self.container.type, self.superclassName, paramStr, self.returnTypeName];
}

- (NSDictionary *)fullDictionary {
  NSString *type = [APIExporterSymbol nameForSymbolType:self.type];
  NSString *minSDK = [NSString stringWithFormat:@"%d.%d", self.minSDKVersion.major, self.minSDKVersion.minor];
  NSString *containerName = [NSString stringWithUTF8String:self.container.containerName];
  NSDictionary *container = @{CONTAINER_NAME_KEY:containerName,
                              CONTAINER_TYPE_KEY:[APIExporterSymbol nameForContainerType:self.container.type],
                              };
  NSDictionary *loc = [self createLocationDict];
  if (!self.returnTypeName)
    self.returnTypeName = @"";
  NSDictionary *info = @{TYPE_KEY:type,
                         NAME_KEY:self.name,
                         MIN_SDK_KEY:minSDK,
                         CONTAINER_KEY:container,
                         LOCATIONS_KEY:@[loc],
                         SUPERCLASS_KEY:self.superclassName,
                         RETURN_TYPE_KEY:self.returnTypeName,
                         PARAMS_KEY:self.params,
                         UNCRITICAL_DECL_KEY:@(self.declarationIsUncritical)
                         };
  return info;
}

// return NO if the symbol's location is already in the dictionary
- (BOOL)mergeIntoDictionary:(NSMutableDictionary *)existingSymbolEntries {
  NSArray *locations = [existingSymbolEntries objectForKey:LOCATIONS_KEY];

  NSDictionary *myLocation = [self createLocationDict];
  for (NSDictionary *location in locations) {
    if ([self locationDictsAreEqual:myLocation compareTo:location]) {
      return NO;
    }
  }
  // myLocation is not equal to any location that is already in the given dictionary

  NSMutableArray *locationsNew = [locations mutableCopy];
  [locationsNew addObject:myLocation];

  [existingSymbolEntries setObject:locationsNew forKey:LOCATIONS_KEY];
  
  return YES;
}

+ (NSString *)nameForContainerType:(AEWSymbolContainerType)type {
  NSArray *types = @[@"Class", @"Protocol", @"Category", @"File", @"None"];
  return types[type];
}

+ (NSString*)nameForSymbolType:(AEWSymbolType)type {
  NSArray *types = @[@"Instance Method", @"Class Method", @"Property", @"Class", @"Subclass", @"C Function", @"Subscripting Method", @"Enum", @"Extern Definition", @"Unknown"];
  return types[type];
}

#pragma mark - Private

- (NSDictionary *)createLocationDict {
  return @{LOCATION_FILE_KEY:[NSString stringWithUTF8String:self.location.filename],
           LOCATION_LINE_KEY:@(self.location.lineNumberInFile)
             };
}

- (BOOL) locationDictsAreEqual:(NSDictionary*)dict1 compareTo:(NSDictionary*)dict2 {

  // for performance/hit rate reasons, compare lines first
  // it is quite likely that there are numerous occurrences in 1 file!
  int line1 = [[dict1 objectForKey:LOCATION_LINE_KEY] integerValue];
  int line2 = [[dict2 objectForKey:LOCATION_LINE_KEY] integerValue];

  if (line1 != line2) {
    return NO;
  }

  NSString *file1 = [dict1 objectForKey:LOCATION_FILE_KEY];
  NSString *file2 = [dict2 objectForKey:LOCATION_FILE_KEY];

  if (![file1 isEqualToString:file2]) {
    return NO;
  }

  return YES;
}

@end

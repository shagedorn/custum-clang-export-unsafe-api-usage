//===----- APIExporterCWrapper.h - Wraps Objective-C Code -*- C++ -------*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
///
/// \file
/// \brief The API Export functionality is implemented in ObjC - this header 
/// defines how to call this functionality from C/C++ code.
///
//===----------------------------------------------------------------------===//

#ifndef Clang_APIExporterCWrapper_h
#define Clang_APIExporterCWrapper_h

#include <vector>
#include <string>

#pragma mark - Typedefs

/// Represents the type of a symbol
typedef enum AEWSymbolType {

  AEWSymbolTypeInstanceMethod,
  AEWSymbolTypeClassMethod,
  AEWSymbolTypeProperty,
  AEWSymbolTypeClass,
  AEWSymbolTypeSubclass,
  AEWSymbolTypeCFunction,
  AEWSymbolTypeSubscriptingMethod,
  AEWSymbolTypeEnum,
  AEWSymbolTypeExternDefinition,
  AEWSymbolTypeUnknown
  
} AEWSymbolType;

/// The source code location where a symbol has been found
// TODO: Switch to string type
typedef struct {

  char filename[512];
  int lineNumberInFile;

} AEWSymbolSourceLocation;

/// The possible types used by the AEWSymbolContainer struct
typedef enum AEWSymbolContainerType {

  AEWSymbolContainerTypeClass,
  AEWSymbolContainerTypeProtocol,
  AEWSymbolContainerTypeCategory,
  AEWSymbolContainerTypeFile,
  AEWSymbolContainerTypeNone

} AEWSymbolContainerType;

/// The container which a symbol was declared in.
// TODO: Switch to string type
typedef struct {

  char containerName[512];
  AEWSymbolContainerType type;

} AEWSymbolContainer;

/// Represents the iOS version
typedef struct {

  int major;
  int minor;

} AEWiOSVersion;

#pragma mark - Functions (will be bridged to ObjC methods)

/// \brief Add a symbol to a temporary collection
///
/// Use "AEWExportSymbols()" to save this collection to a PLIST file
///
/// \param symbolType               The symbol's type
///
/// \param sourceCodeLocation       Where the symbol was found
///
/// \param introducedIniOSVersion   The minimum SDK version the symbol
///                                 requires.
///
/// \param symbolName               The symbol's name
///
/// \param symbolContainer          The container which the symbol was
///                                 declared in.
///
/// \param superClassName           (optional) The name of the symbol's
///                                 superclass, if the symbol is of type
///                                 'Subclass'
///
/// \param returnTypeName           (optional) The string representation
///                                 of the return type for methods and
///                                 functions
///
/// \param params                   The parameters of a function or method.
///                                 Includes parameter type and name.
///
/// \param declaredByUncriticalClass (optional) Set this flag if the
///                                 symbol is only critical because of
///                                 its receiver, but the declaration
///                                 itself is uncritical.
///
void AEWAddSymbol(AEWSymbolType symbolType,
                  AEWSymbolSourceLocation sourceCodeLocation,
                  AEWiOSVersion introducedIniOSVersion,
                  const char* symbolName,
                  AEWSymbolContainer symbolContainer,
                  const char* superClassName,
                  std::string returnTypeName,
                  std::vector< std::vector<std::string> > params,
                  bool declaredByUncriticalClass);

/// \brief Export previously collected symbols to disk
///
/// Use 'AEWAddSymbol()' to collect symbols.
///
/// \param path   A base path which locates the build directory.
///               The current implementation relies on Xcode's
///               path structure and does not use the value of
///               'path' directly. The symbol information is
///               written to a modified version of 'path'.
///
void AEWExportSymbols(const char* path);

#endif

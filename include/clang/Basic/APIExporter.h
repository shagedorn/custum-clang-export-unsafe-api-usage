//===- APIExporter.h - Replicates APIExporterCWrapper for ObjC -*- ObjC -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
///
/// \file
/// \brief Defines export API for Objective-C
///
//===----------------------------------------------------------------------===//

#ifndef CLANG_BASIC_APIEXPORTER_H
#define CLANG_BASIC_APIEXPORTER_H

#import <Foundation/Foundation.h>

#import "clang/basic/APIExporterCWrapper.h"
#import "clang/basic/APIExporterSymbol.h"

#pragma mark - Public Interface

/// \brief For documentation, see 'APIExorterCWrapper.h'
///
/// This header is only used to bridge between C++ and ObjC
///
@interface APIExporter : NSObject {}

- (void) addSymbol:(APIExporterSymbol*)symbolToAdd;
- (void) exportSymbols:(NSString*)path;

@end

#endif


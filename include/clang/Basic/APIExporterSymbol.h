//===--- APIExporterSymbol.h - Representation of an API symbol -*- ObjC -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
///
/// \file
/// \brief Defines a symbol that may be exported for analysis purposes.
///
//===----------------------------------------------------------------------===//

#ifndef CLANG_BASIC_APIEXPORTER_SYMBOL_H
#define CLANG_BASIC_APIEXPORTER_SYMBOL_H

#import <Foundation/Foundation.h>
#import "clang/basic/APIExporterCWrapper.h"

#pragma mark - Keys for the dictionary representation

#define TYPE_KEY @"type"
#define NAME_KEY @"name"
#define MIN_SDK_KEY @"minSDK"
#define CONTAINER_KEY @"container"
  #define CONTAINER_NAME_KEY @"c_name"
  #define CONTAINER_TYPE_KEY @"c_type"
#define LOCATIONS_KEY @"locations"
  #define LOCATION_FILE_KEY @"l_file"
  #define LOCATION_LINE_KEY @"l_line"
#define SUPERCLASS_KEY @"superclass"
#define RETURN_TYPE_KEY @"return_type"
#define PARAMS_KEY @"params"
#define UNCRITICAL_DECL_KEY @"uncritical_decl"

#pragma mark - Public Interface

/**
 *  This class represents one source code symbol, e.g., a
 *  method, class, or function.
 */
@interface APIExporterSymbol : NSObject

#pragma mark - Typed properties for one symbol

/**
 *  The type of the symbol, e.g., a method or a function.
 */
@property (assign) AEWSymbolType type;

/**
 *  The container in which the symbol was originally
 *  declared (not used!), e.g., a class interface or a protocol.
 */
@property (assign) AEWSymbolContainer container;

/**
 *  The location at which a symbol has been found. (i.e.,
 *  where it is referenced)
 */
@property (assign) AEWSymbolSourceLocation location;

/**
 *  The SDK version which first introduced the symbol.
 */
@property (assign) AEWiOSVersion minSDKVersion;

/**
 *  The symbol's full name.
 *
 *  For methods, this equals the selector.
 */
@property (copy) NSString *name;

/**
 *  If the symbol's type is 'Subclass', the name of the
 *  superclass should be provided.
 */
@property (copy) NSString *superclassName;

/**
 *  The string representation of the return type for
 *  functions and methods.
 */
@property (copy) NSString *returnTypeName;

/**
 *  The parameters of C functions and methods.
 *
 *  Each entry is another array. The type is stored
 *  as index 0, the parameter name at index 1. Both
 *  are of type NSString.
 */
@property (strong) NSArray *params;

/**
 *  Normally set to NO. Example where this could
 *  be set to YES:
 *
 *  -'description' was called on critical receiver
 *  -'description' is therefore considered critical
 *  -however, 'description' is declared in NSObject,
 *  so sending this message to any proxy object 
 *  would not crash, but may not result in the
 *  desired behaviour.
 */
@property (assign) BOOL declarationIsUncritical;

#pragma mark - Export and identification properties

/**
 *  Key containing type, name, minSDK and container fields.
 *
 *  Should uniquely identify one type of symbol, but not
 *  the source location where it was referenced. It may
 *  include it's declaration location via the container.
 */
- (NSString*) symbolKey;

/**
 *  Returns a dictionary with all the symbol's information,
 *  having exactly one source code location
 */
- (NSDictionary*) fullDictionary;

/**
 *  Adds this symbol's source code location to the existing
 *  dictionary, if not already present.
 *
 *  Returns NO if this location was already present.
 */
- (BOOL) mergeIntoDictionary:(NSMutableDictionary*)existingSymbolEntries;

/**
 *  Descriptive name for the type, e.g., "Method"
 */
+ (NSString*) nameForSymbolType:(AEWSymbolType)type;

/**
 *  Descriptive name for the container's type, e.g., "Category"
 */
+ (NSString*) nameForContainerType:(AEWSymbolContainerType)type;

@end

#endif

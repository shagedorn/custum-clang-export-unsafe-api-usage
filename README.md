# C Language Family Front-end #

## Extension: Unsafe API Checks (iOS & Mac applications only) ##

### Deprecation Notice

This project is not maintained and may include outdated information or code that is
not compatible with recent versions of tools, such as, Xcode and Clang. The
repository is online for reference only.

If you are interested in this project, you may want to read 
[this brief answer on Stackoverflow](https://stackoverflow.com/questions/34013293/why-does-objective-c-not-have-api-availability-checking/34040901#34040901),
detailing the current state of affairs. If you speak German, you can download & read 
my [diploma thesis](http://www.hagi-dd.de/docs/Diplomarbeit.pdf) in full. Please keep
in mind that it was written in 2013, any many things (Swift, Clang, Xcode,...) have
changed since it was published.

Information on how to use custom (or bleeding edge) Clang versions in Xcode
is summarized in these Stackoverflow posts:

* [Is it possible to compile a newer version of LLVM and use it with Xcode?](https://stackoverflow.com/questions/16261612/is-it-possible-to-compile-a-newer-version-of-llvm-and-use-it-with-xcode/16265711#16265711)
* [How do I make Xcode use an alternative version of clang?](https://stackoverflow.com/questions/23071024/how-do-i-make-xcode-use-an-alternative-version-of-clang/23079653#23079653)

For current projects, please visit my [GitHub](https://github.com/shagedorn) or
[Stackoverflow](https://stackoverflow.com/users/2050985/hagi) profiles.

### Description

This extension brings support to detect so-called **unsafe APIs**, or **critical APIs**.
Any API that was introduced later than the current deployment target (minimum runtime 
version) is considered *unsafe* as it may not be available on all platforms that the app
promises to support.

The extension has been developed for **iOS applications** and should work, but has
not been tested, for Mac applications as well. It relies on availability
information that Apple provides using availability macros in the framework
headers. The extension is **not cross-platform compatible** as it dynamically
links the Foundation framework that ships with Mac OS X only.

It supports the detection of these types of (critical) symbols and APIs:

*	Objective-C methods, including the usage of `performSelector:`
*	Objective-C classes and superclasses
*	C functions
*	String constants that are declared `extern`

By default (i.e., on branch `customised_clang`), the extension does not raise warnings 
nor does it export any information. Use these flags to generate warnings and output:

*	`-Wunsafe-api-usage`: Show warnings for all supported unsafe symbols
*	`-export-api-usage`: Export (un)availability information of all critical symbols
to a PLIST file within the build directory
*	`-Wfix-unsafe-api-usage`: Suggest fixes. Not recommended for general purpose usage
as it relies on conventions from 
[another project](https://bitbucket.org/shagedorn/unsafe-api-resolver).
*	`-Wunfixable-api-usage`: Only show warnings that the 
[above-mentioned project](https://bitbucket.org/shagedorn/unsafe-api-resolver)
cannot resolve

**Note:** Settings custom warning flags may cause compatibility issues with Xcode (it breaks
autocomplete, unless you overwrite the default `clang` binary *and* `libClang.dylib`
in Xcode's default Toolchain:
`/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/`)

To allow the use of this customised version simply by setting the `CC` flag (leaving
the existing toolchain untouched), two branches with different sets of default-enabled
warnings have been created:

+ `customised_clang_detection`: Enable `-Wunsafe-api-usage` by default.
+ `customised_clang_resolution`: Enable `-Wfix-unsafe-api-usage` and 
`-Wunfixable-api-usage` by default.

Builds of both branches and a tool to easily integrate it into your own Xcode project
are provided in a [separate repository](https://bitbucket.org/shagedorn/unsafe-api-resolver).

Extension developed by Sebastian Hagedorn.

## Original Clang README ##

Welcome to Clang.  This is a compiler front-end for the C family of languages
(C, C++, Objective-C, and Objective-C++) which is built as part of the LLVM
compiler infrastructure project.

Unlike many other compiler frontends, Clang is useful for a number of things
beyond just compiling code: we intend for Clang to be host to a number of
different source level tools.  One example of this is the Clang Static Analyzer.

If you're interested in more (including how to build Clang) it is best to read
the relevant web sites.  Here are some pointers:

Information on Clang:              http://clang.llvm.org/
Building and using Clang:          http://clang.llvm.org/get_started.html
Clang Static Analyzer:             http://clang-analyzer.llvm.org/
Information on the LLVM project:   http://llvm.org/

If you have questions or comments about Clang, a great place to discuss them is
on the Clang development mailing list:
  http://lists.cs.uiuc.edu/mailman/listinfo/cfe-dev

If you find a bug in Clang, please file it in the LLVM bug tracker:
  http://llvm.org/bugs/
